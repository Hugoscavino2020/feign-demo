package com.cw.feign;

import feign.Response;

public class CwRestApiServerException extends RuntimeException{

    private final String requestUrl;

    private final int internalErrorCode;

    private final Response.Body body;

    public CwRestApiServerException(String requestUrl, Response.Body responseBody, int internalErrorCode) {
        this.requestUrl = requestUrl;
        this.internalErrorCode = internalErrorCode;
        this.body = responseBody;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public Response.Body getBody() {
        return body;
    }

    public int getInternalErrorCode() {
        return internalErrorCode;
    }
}
