package com.cw.feign.config;

import com.cw.feign.ErrorDecoder.CwCustomErrorDecoder;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class CwFeignClientConfiguration {
    @Bean
    public ErrorDecoder errorDecoder() {
        return new CwCustomErrorDecoder();
    }
}
