package com.cw.feign.ErrorDecoder;

import com.cw.feign.CwRestApiServerException;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

/**
 * Taken from
 * <a href="https://stackoverflow.com/questions/55020389/spring-feign-client-exception-handling">Custom ErrorDecoder</a>
 */

public class CwCustomErrorDecoder implements ErrorDecoder {
    public static final int SOME_CRITICAL_ERROR_CODE = 42;
    public static final int SOME_VENDOR_NON_CRITICAL_ERROR_CODE = 1001;
    private final Logger log = LoggerFactory.getLogger(CwCustomErrorDecoder.class);
    @Override
    public Exception decode(String methodKey, Response response) {
        final String requestUrl = response.request().url();
        final Response.Body responseBody = response.body();
        final HttpStatus responseStatus = HttpStatus.valueOf(response.status());

        log.error("CwCustomErrorDecoder has been invoked");
        if (responseStatus.is5xxServerError()) {
            log.error("CwCustomErrorDecoder : is5xxServerError responseStatus " + responseStatus.value());
            return new CwRestApiServerException(requestUrl, responseBody, SOME_CRITICAL_ERROR_CODE);
        } else if (responseStatus.is4xxClientError()) {
            log.error("CwCustomErrorDecoder : is4xxClientError responseStatus " + responseStatus.value());
            return new CwRestApiServerException(requestUrl, responseBody, SOME_VENDOR_NON_CRITICAL_ERROR_CODE);
        } else {
            log.error("CwCustomErrorDecoder : Unknown responseStatus " + responseStatus.value());
            return new Exception("Generic exception");
        }
    }
}