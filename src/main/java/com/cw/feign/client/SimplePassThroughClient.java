package com.cw.feign.client;

import com.cw.core.model.SimpleClientData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Client calls back to core API at the configured URL
 * <strong>Note: FeignClient url is set in application.properties</strong>
 */
@FeignClient(name = "simple-client")
public interface SimplePassThroughClient {
    @GetMapping("/data/{dataId}")
    SimpleClientData getData(@PathVariable long dataId);
}