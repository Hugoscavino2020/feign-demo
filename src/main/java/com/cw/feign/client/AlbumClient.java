package com.cw.feign.client;

import com.cw.core.model.AlbumClientData;
import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;

import java.net.URI;

@FeignClient(name = "album-client")
public interface AlbumClient {
    @RequestLine(value = "GET")
    AlbumClientData getAlbumByIdAndUrl(URI url);
}
