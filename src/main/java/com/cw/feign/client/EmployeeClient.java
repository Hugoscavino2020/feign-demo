package com.cw.feign.client;

import com.cw.core.model.EmployeeClientData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.net.URI;

/**
 * The URL is configured manually by controller
 */
@FeignClient(name = "employee-client")
public interface EmployeeClient {
    @GetMapping
    EmployeeClientData getEmployeeByUri(URI uri);
}