package com.cw.feign.client;

import com.cw.core.model.SimpleClientData;
import com.cw.feign.config.CwFeignClientConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "http-error-client", configuration = CwFeignClientConfiguration.class, url = "http://localhost:8080/")
public interface ProxyHttpErrorClient {
    @GetMapping("/data-with-error/{id}")
    SimpleClientData getData(@PathVariable long id);
}
