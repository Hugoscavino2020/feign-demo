package com.cw.feign.controller;

import com.cw.core.model.SimpleClientData;
import com.cw.feign.CwRestApiServerException;
import com.cw.feign.ErrorDecoder.CwCustomErrorDecoder;
import com.cw.feign.client.ProxyHttpErrorClient;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class HttpErrorController {

    private final ProxyHttpErrorClient errorClient;

    private final Logger log = LoggerFactory.getLogger(HttpErrorController.class);

    @GetMapping("/bad-data/{id}")
    public ResponseEntity<SimpleClientData> getBadData(@PathVariable long id) {

        SimpleClientData data;

        try {
            data = errorClient.getData(id);
        }
        catch (CwRestApiServerException exception){
            if (exception.getInternalErrorCode() == CwCustomErrorDecoder.SOME_CRITICAL_ERROR_CODE){
                log.warn("Something Bad Happened : " + exception.getMessage());
                return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
            } else if (exception.getInternalErrorCode() == CwCustomErrorDecoder.SOME_VENDOR_NON_CRITICAL_ERROR_CODE){
                log.warn("Something Bad Happened But We Do Not Care - Just Log it: " + exception.getMessage());
                return new ResponseEntity<>(new SimpleClientData(id, "Warning You", 0), HttpStatus.I_AM_A_TEAPOT);
            } else {
                log.info("Something Bad Happened But We Do Not Care - Just Log it: ");
                return new ResponseEntity<>(new SimpleClientData(id, "Ignore Me", 0), HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(data, HttpStatus.OK);
    }
}
