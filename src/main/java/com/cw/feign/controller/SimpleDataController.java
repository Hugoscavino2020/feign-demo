package com.cw.feign.controller;

import com.cw.core.model.SimpleClientData;
import com.cw.feign.client.SimplePassThroughClient;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class SimpleDataController {

    private final SimplePassThroughClient client;


    @GetMapping("/data/{id}")
    public SimpleClientData getData(@PathVariable long id) {
        return client.getData(id);
    }


}