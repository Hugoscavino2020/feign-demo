package com.cw.feign.controller;

import com.cw.core.model.AlbumClientData;
import com.cw.feign.client.AlbumClient;
import feign.Feign;
import feign.Target;
import feign.codec.Decoder;
import feign.codec.Encoder;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

@RestController
@Import(FeignClientsConfiguration.class)
public class AlbumFeignClientController {

    private final AlbumClient client;
    public AlbumFeignClientController(Decoder decoder, Encoder encoder) {
        this.client = Feign.builder()
                .encoder(encoder)
                .decoder(decoder)
                .target(Target.EmptyTarget.create(AlbumClient.class));
    }

    @GetMapping(value = "/music-album/{id}")
    public AlbumClientData getTodoById(@PathVariable(value = "id") Long id) {
        final String HTTP_FILE_URL = "http://localhost:8080/album/";
        return client.getAlbumByIdAndUrl(URI.create(HTTP_FILE_URL + id));
    }

}
