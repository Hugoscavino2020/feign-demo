package com.cw.feign.controller;


import com.cw.feign.client.EmployeeClient;
import com.cw.core.model.EmployeeClientData;

import feign.Feign;
import feign.Target;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.ObjectProvider;


import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.cloud.openfeign.support.HttpMessageConverterCustomizer;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

@RestController
@Import(FeignClientsConfiguration.class)
public class EmployeeManualController {

    private final ObjectFactory<HttpMessageConverters> messageConverters;

    private final ObjectProvider<HttpMessageConverterCustomizer> customizers;

    private final EmployeeClient employeeClient;
    public EmployeeManualController(ObjectFactory<HttpMessageConverters> messageConverters, ObjectProvider<HttpMessageConverterCustomizer> customizers) {
        this.messageConverters = messageConverters;
        this.customizers = customizers;
        this.employeeClient =  Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(messageConverters))
                .decoder(new SpringDecoder(messageConverters, customizers))
                .target(Target.EmptyTarget.create(EmployeeClient.class));
    }
    @GetMapping("/employee/{id}")
    public EmployeeClientData getEmployee(@PathVariable("id") long id) {
        final String HTTP_FILE_EMPLOYEE_URL = "http://localhost:8080/employee/";
        return employeeClient.getEmployeeByUri(URI.create(HTTP_FILE_EMPLOYEE_URL + id));
    }
}