package com.cw.core;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.stereotype.Component;


@Component
public class CustomContainer implements WebServerFactoryCustomizer<TomcatServletWebServerFactory> {

    @Value("${simple-server.port:8080}")
    private int port;

    @Override
    public void customize(TomcatServletWebServerFactory factory) {
        factory.setPort(port);
    }
}