package com.cw.core.controller;

import com.cw.core.model.EmployeeClientData;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {

    @GetMapping("/employee/{id}")
    public EmployeeClientData getEmployee(@PathVariable long id) {
        return new EmployeeClientData(id, "jane.doe");
    }
}
