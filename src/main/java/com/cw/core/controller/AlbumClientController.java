package com.cw.core.controller;

import com.cw.core.model.AlbumClientData;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AlbumClientController {

    @GetMapping("/album/{id}")
    public AlbumClientData getAlbum(@PathVariable long id) {
        return new AlbumClientData(id, "rock-rb-country");
    }
}
