package com.cw.core.controller;

import com.cw.core.model.SimpleClientData;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AlwaysThrowHttpErrorController {

    @GetMapping("/data-with-error/{id}")
    public ResponseEntity<SimpleClientData> getBadData(@PathVariable long id) {
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
