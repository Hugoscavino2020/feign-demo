package com.cw.core.controller;

import com.cw.core.model.SimpleClientData;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SimpleClientController {

    /**
     * We have a simple controller with one endpoint inside, for fetching data for a particular id.
     * In our example, we will just create some kind of fake object using the provided id, and return the
     * SimpleClientData object to the caller as JSON.
     * That is all for our client, it is enough for us to show Feign usage.
     * Time for more interesting stuff in the second service.
     *
     * @param id Fake ID
     * @return Fake SimpleClientData
     */
    @GetMapping("/data/{id}")
    public SimpleClientData getData(@PathVariable long id) {
        return new SimpleClientData(id, "name-" + id, id * 2);
    }
}