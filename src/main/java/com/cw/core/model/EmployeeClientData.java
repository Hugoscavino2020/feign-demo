package com.cw.core.model;

public record EmployeeClientData(Long id, String name) {}