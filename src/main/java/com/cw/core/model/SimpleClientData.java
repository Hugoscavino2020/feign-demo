package com.cw.core.model;

/**
 * To simplify, we have used Java Record which generates for us an all arguments constructor we will use in the
 * controller. As for the fields, we will have id, name, and amount, nothing really out of ordinary here.
 */
public record SimpleClientData(long id, String name, double amount) {}