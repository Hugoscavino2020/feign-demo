package com.cw.core.model;

public record AlbumClientData (Long id, String name) {}