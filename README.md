# Feign Demo

#### Sample Programs Inspired by

    https://springframework.guru/feign-rest-client-for-spring-application/

## Core APIs Calls

### AlbumClientController

Link to [AlbumClientController](http://localhost:8080/album/1)

    @GetMapping("/album/{id}")

    curl http://localhost:8080/album/1

#### Response
`{
"id": 1,
"name": "rock-rb-country"
}`

### EmployeeController

Link to [EmployeeController](http://localhost:8080/employee/1)

    @GetMapping("/employee/{id}")

    curl http://localhost:8080/employee/1

#### Response

`{
"id": 1,
"name": "jane.doe"
}`

### SimpleClientController

Link to [SimpleClientController](http://localhost:8080/data/1)

    @GetMapping("/data/{id}")

    curl http://localhost:8080/data/1

#### Response

`{
"id": 1,
"name": "name-1",
"amount": 2
}`

### AlwaysThrowHttpErrorController 

Link to [AlwaysThrowHttpErrorController](http://localhost:8080/data-with-error/1)

    @GetMapping("/data-with-error/{id}")

    curl http://localhost:8080/data-with-error/1

#### Response

`500 Internal Server Error`

----------

## Feign APIs Calls

### SimpleDataController : port 8081

Link to [SimpleDataController](http://localhost:8081/data/1)

    @GetMapping("/data/{id}")

    curl http://localhost:8081/data/1

#### Response

`{
"id": 1,
"name": "name-1",
"amount": 2
}`

### HttpErrorController : port 8081

Link to [HttpErrorController](http://localhost:8081/bad-data/1)

    @GetMapping("/bad-data/{id}")

    curl http://localhost:8081/bad-data/1

#### Response

`{
"id": 1,
"name": "name-1",
"amount": 2
}`